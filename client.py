import socket
import threading
import pyaudio
import queue
import audioop
from config import AUDIO_PART, ID_SIZE, NAME_SIZE, SILENCE_RMS, ACTIVATION_RMS


class Client:
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.queue_size = 10
        self.handle_inputs()
        audio_format = pyaudio.paInt16

        self.p = pyaudio.PyAudio()
        self.playing_stream = self.p.open(format=audio_format, channels=1, rate=4000, output=True, frames_per_buffer=AUDIO_PART)
        self.recording_stream = self.p.open(format=audio_format, channels=1, rate=4000, input=True, frames_per_buffer=AUDIO_PART)
        try:
            self.s.send(self.session_id)
            self.s.send(self.name[:NAME_SIZE])
            threading.Thread(target=self.receive).start()
            self.send()
        except Exception as e:
            print('Unexpected error occurred', e)

    def is_data_buffer_empty(self):
        return self.data_buffer.qsize() == 0
    
    def handle_inputs(self):
        print("Enter your name:")
        self.name = bytes(input(), encoding='utf-8')
        print("Enter session_id:")
        self.session_id = bytes(input(), encoding='utf-8')[:ID_SIZE]
        self.session_id = self.session_id.rjust(ID_SIZE, b'0')
        while True:
            try:
                print("Enter server ip:")
                self.target_ip = input()
                print("Enter server port:")
                self.target_port = int(input())
                self.s.connect((self.target_ip, self.target_port))
                break
            except:
                print("Error while connecting to the server")
        print("Succeccfully connected to the server")

    def receive(self):
        while True:
            try:
                data = self.s.recv(AUDIO_PART)
                self.playing_stream.write(data)
            except:
                pass

    def send(self):
        self.data_buffer = queue.Queue()
        current_average_value = 0
        while True:
            try:
                data = self.recording_stream.read(AUDIO_PART)
                average_value = audioop.rms(data, 2)
                if not is_data_buffer_empty():
                    data_buffer.put(average_value, block=False)
                    current_average_value += average_value
                    if data_buffer.qsize() => SLIDING_MEAN:
                        current_average_value -= data_buffer.get(block=False)
                        if current_average_value / data_buffer.qsize() < SILENCE_RMS:
                            data_buffer = queue.Queue()
                        else:
                            self.s.sendall(data)
                    else:
                        self.s.sendall(data)
                elif average_value > ACTIVATION_RMS:
                    data_buffer.put(average_value, block=False)
                    current_average_value += average_value
                    self.s.sendall(data)
            except:
                self.s.close()
                print('Error while handling the session ended', flush=True)
                break


client = Client()
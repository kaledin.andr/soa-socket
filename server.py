import socket
import threading
import time
import numpy as np
from config import AUDIO_PART, ID_SIZE, NAME_SIZE


class Session:
    def __init__(self, session_id):
        self.session_id = session_id
        self.users_map = {}
        self.session_lock = threading.Lock()

    def handle_connection(self, address, sock):
        while True:
            try:
                data = sock.recv(AUDIO_PART)
                self.send_to_all(address, data)
            except socket.error:
                sock.close()
                with threading.Lock():
                    print("Connection finished!")
                    print("\tsession_id = ", self.session_id, "; addressess = ", address)
                break

    def send_to_all(self, address, data):
        with self.session_lock:
            for user_address, user in self.users_map.items():
                if user_address != address:
                    user.add_to_buffer(data)

    def add_user(self, sock, address):
        with self.session_lock:
            self.users_map[address] = AudioClient(self.session_id, sock, address, self.remove_user)
            self.users_map[address].run()
            threading.Thread(target=self.handle_connection, args=(address, sock)).start()

    def remove_user(self, address):
        with self.session_lock:
            self.users_map.pop(address)


class AudioClient:
    def __init__(self, session_id, sock, address, action_on_delete):
        self.session_id = session_id
        self.sock = sock
        self.address = address
        self.action_on_delete = action_on_delete
        
        self.batches = []
        self.buffer_lock = threading.Lock()
        self.last_time = time.time()

    def is_buffer_empty(self):
        return len(self.batches) > 0

    def init_sending_thread(self):
        data = np.array(self.batches, dtype='int16')
        data = data.mean(axis=0, dtype='int16')
        threading.Thread(target=self.send, args=(data,)).start()
        self.batches = []

    def run(self):
        threading.Thread(target=self.process_audio, args=()).start()

    def send(self, data):
        try:
            self.sock.sendall(data)
        except:
            self.sock.close()

    def add_to_buffer(self, batch):
        with self.buffer_lock:
            self.batches.append(np.frombuffer(batch, dtype='int16'))

    def process_audio(self):
        while not self.sock._closed:
            if not is_buffer_empty():
                with self.buffer_lock:
                    self.init_sending_thread()
        self.action_on_delete(self.address)


class Server:
    def __init__(self):
        self.address_to_session_map = {}
        self.session_lock = threading.Lock()

        self.ip = socket.gethostbyname(socket.gethostname())
        while True:
            try:
                print("Enter port:")
                self.port = int(input())
                self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                bind_info = (self.ip, self.port)
                self.sock.bind(bind_info)
                break
            except:
                print("Error while binding to this port")
        self.sock.listen(100)
        print("Started: ip = ", self.ip, "; port = ", str(self.port))
        while True:
            c, address = self.sock.accept()
            threading.Thread(target=self.handle_connection, args=(c, address)).start()

    def handle_connection(self,c,address):
        try:
            session_id, username = c.recv(ID_SIZE), c.recv(NAME_SIZE)
        except:
            with threading.Lock():
                print("User initialisation failed")
            return

        with threading.Lock():
            print("Username = ", username, "; with session_id = ", session_id)
        with self.session_lock:
            if session_id in self.address_to_session_map:
                self.address_to_session_map[session_id].add_user(c, address)
            else:
                self.address_to_session_map[session_id] = Session(session_id)
                self.address_to_session_map[session_id].add_user(c, address)

    def remove_user(self, address, session_id):
        with self.session_lock:
            self.address_to_session_map[session_id].pop(address)
            if not self.address_to_session_map[session_id]:
                self.address_to_session_map.pop(session_id)


server = Server()
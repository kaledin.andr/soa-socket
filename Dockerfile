FROM python:3.8-slim-buster

COPY . .

RUN pip install -r requirements.txt
RUN apt-get install libportaudio2 libportaudiocpp0 portaudio19-dev libasound-dev  -y

CMD python3 -u server.py